# ORY Hydra OAuth 2.0 + OpenID Connect Client Provider for The PHP League OAuth2-Client
[ORY Hydra](https://www.ory.sh/hydra/) OAuth 2.0 + OpenID Connect support for the PHP League's 
[OAuth 2.0 Client](https://github.com/thephpleague/oauth2-client).