<?php


namespace Vallarj\OAuth2\Client\Exception;


class InvalidTokenException extends Exception
{
}
