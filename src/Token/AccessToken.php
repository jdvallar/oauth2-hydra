<?php


namespace Vallarj\OAuth2\Client\Token;


use Lcobucci\JWT\Encoding\JoseEncoder;
use Lcobucci\JWT\Token;
use Lcobucci\JWT\UnencryptedToken;
use League\OAuth2\Client\Token\AccessToken as LeagueAccessToken;

class AccessToken extends LeagueAccessToken
{
    /** @var UnencryptedToken */
    private $idToken;

    /** @var IdTokenValidator */
    private $validator;

    /**
     * AccessToken constructor.
     *
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        parent::__construct($options);
        $tokenParser = new Token\Parser(new JoseEncoder());

        if (!empty($this->values['id_token'])) {
            $this->idToken = $tokenParser->parse($this->values['id_token']);
            unset($this->values['id_token']);
        }
    }

    /**
     * Returns the ID Token
     *
     * @return Token
     */
    public function getIdToken(): Token
    {
        return $this->idToken;
    }

    /**
     * Returns the ID token validator
     *
     * @return IdTokenValidator
     */
    public function getIdTokenValidator(): IdTokenValidator
    {
        if (!isset($this->validator)) {
            $this->validator = new HydraIdTokenValidator();
        }

        return $this->validator;
    }

    /**
     * Returns true if ID token is valid
     *
     * @param string $publicKey
     * @param string|null $nonce
     * @param string $issuer
     * @param string $audience
     * @return bool
     */
    public function hasValidIdToken(
        string $publicKey,
        ?string $nonce,
        string $issuer,
        string $audience
    ): bool {
        $validator = $this->getIdTokenValidator();
        return !is_null($this->idToken) && $validator->validate($this->idToken, $publicKey, $nonce, $issuer, $audience);
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        $parameters = parent::jsonSerialize();
        if ($this->idToken) {
            $parameters['id_token'] = $this->idToken->toString();
        }

        return $parameters;
    }
}
