<?php


namespace Vallarj\OAuth2\Client\Token;


use DateTimeImmutable;
use Lcobucci\JWT\Signer;
use Lcobucci\JWT\UnencryptedToken;
use Lcobucci\JWT\Validation\Constraint\SignedWith;
use Lcobucci\JWT\Validation\ConstraintViolation;

class HydraIdTokenValidator implements IdTokenValidator
{
    /* Clock skew of 5 minutes */
    private const SKEW = 60 * 5;

    /**
     * @inheritDoc
     */
    public function validate(
        UnencryptedToken $token,
        string $publicKey,
        ?string $nonce,
        string $issuer,
        string $clientId
    ): bool {
        // ID Token validation
        // https://openid.net/specs/openid-connect-core-1_0.html#IDTokenValidation

        // 3.1.3.7.6 - 3.1.3.7.7
        // If the ID Token is received via direct communication between the Client and the Token Endpoint
        // (which it is in this flow), the TLS server validation MAY be used to validate the issuer in place of
        // checking the token signature. The Client MUST validate the signature of all other ID Tokens according
        // to JWS [JWS] using the algorithm specified in the JWT alg Header Parameter. The Client MUST use the
        // keys provided by the Issuer.
        // The alg value SHOULD be the default of RS256 or the algorithm sent by the Client in the
        // id_token_signed_response_alg parameter during Registration.
        $key = Signer\Key\InMemory::plainText($publicKey);
        $signer = new Signer\Rsa\Sha256();
        $signedWithConstraint = new SignedWith($signer, $key);
        try {
            $signedWithConstraint->assert($token);
        } catch (ConstraintViolation $_) {
            return false;
        }

        $claims = $token->claims();

        // 3.1.3.7.2
        // The Issuer Identifier for the OpenID Provider (which is typically obtained during Discovery)
        // MUST exactly match the value of the iss (issuer) Claim
        if (!$claims->has('iss') || $issuer !== $claims->get('iss')) {
            return false;
        }

        // 3.1.3.7.3
        // The Client MUST validate that the aud (audience) Claim contains its client_id value registered
        // at the Issuer identified by the iss (issuer) Claim as an audience. The aud (audience) Claim
        // MAY contain an array with more than one element. The ID Token MUST be rejected if the ID Token
        // does not list the Client as a valid audience, or if it contains additional audiences not
        // trusted by the Client.
        if (!$claims->has('aud')) {
            return false;
        }

        $aud = $claims->get('aud');
        if (!is_array($aud)) {
            if ($aud !== $clientId) {
                return false;
            }
        } else if (!in_array($clientId, $aud)) {
            return false;
        }

        // 3.1.3.7.4
        // If the ID Token contains multiple audiences, the Client SHOULD verify that an azp Claim is present.
        if (is_array($aud) && count($aud) > 1 && !$claims->has('azp')) {
            return false;
        }

        // 3.1.3.7.5
        // If an azp (authorized party) Claim is present, the Client SHOULD verify that its client_id
        // is the Claim Value.
        if ($claims->has('azp') && $clientId !== $claims->get('azp')) {
            return false;
        }

        // 3.1.3.7.9
        // The current time MUST be before the time represented by the exp Claim
        if (!$claims->has('exp')) {
            return false;
        } else {
            $time = time();
            /** @var DateTimeImmutable $exp */
            $exp = $claims->get('exp');
            if ($time >= $exp->getTimestamp()) {
                return false;
            }
        }

        // 3.1.3.7.10 - Not Implemented
        // The iat Claim can be used to reject tokens that were issued too far away from the current time,
        // limiting the amount of time that nonces need to be stored to prevent attacks. The acceptable
        // range is Client specific.
        // --

        // 3.1.3.7.11
        // If a nonce value was sent in the Authentication Request, a nonce Claim MUST be present and its value
        // checked to verify that it is the same value as the one that was sent in the Authentication Request.
        // The Client SHOULD check the nonce value for replay attacks. The precise method for detecting replay
        // attacks is Client specific.
        if ($nonce != ($claims->has('nonce') ? $claims->get('nonce') : null)) {
            return false;
        }

        // 3.1.3.7.12 - Not Implemented
        // If the acr Claim was requested, the Client SHOULD check that the asserted Claim Value is appropriate.
        // The meaning and processing of acr Claim Values is out of scope for this specification.
        // --

        // RFC 7519 - 4.1.5
        // The nbf (not before) claim identifies the time before which the token MUST NOT be accepted for processing.
        // The processing of the nbf claim requires that the current date/time MUST be after or equal to the
        // not-before date/time listed in the nbf claim. Implementers MAY provide for some small leeway,
        // usually no more than a few minutes, to account for clock skew. This claim is OPTIONAL.
        if ($claims->has('nbf')) {
            /** @var DateTimeImmutable $nbf */
            $nbf = $claims->get('nbf');
            if (($time + self::SKEW) < $nbf->getTimestamp()) {
                return false;
            }
        }

        return true;
    }
}
