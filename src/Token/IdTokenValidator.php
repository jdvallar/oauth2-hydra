<?php


namespace Vallarj\OAuth2\Client\Token;


use Lcobucci\JWT\UnencryptedToken;

interface IdTokenValidator
{
    /**
     * Returns true if Token is valid
     *
     * @param UnencryptedToken $token
     * @param string $publicKey
     * @param string|null $nonce
     * @param string $issuer
     * @param string $clientId
     * @return bool
     */
    public function validate(
        UnencryptedToken $token,
        string $publicKey,
        ?string $nonce,
        string $issuer,
        string $clientId
    ): bool;
}
