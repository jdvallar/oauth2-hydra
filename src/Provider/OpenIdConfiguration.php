<?php


namespace Vallarj\OAuth2\Client\Provider;


interface OpenIdConfiguration
{
    /**
     * Returns the issuer
     *
     * @return string
     */
    public function getIssuer(): string;

    /**
     * Returns the authorization endpoint
     *
     * @return string
     */
    public function getAuthorizationEndpoint(): string;

    /**
     * Returns the token endpoint
     *
     * @return string
     */
    public function getTokenEndpoint(): string;

    /**
     * Returns the userinfo endpoint
     *
     * @return string
     */
    public function getUserinfoEndpoint(): string;

    /**
     * Returns the revocation endpoint
     *
     * @return string
     */
    public function getRevocationEndpoint(): string;

    /**
     * Returns the end session endpoint
     *
     * @return string
     */
    public function getEndSessionEndpoint(): string;

    /**
     * Returns the OpenID public key
     *
     * @return string
     */
    public function getPublicKey(): string;
}
