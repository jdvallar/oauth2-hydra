<?php


namespace Vallarj\OAuth2\Client\Provider;


use CoderCat\JWKToPEM\Exception\Base64DecodeException;
use CoderCat\JWKToPEM\Exception\JWKConverterException;
use CoderCat\JWKToPEM\JWKConverter;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

class OIDCDiscovery implements OpenIdConfiguration
{
    private const CONFIG_NAME = 'config.json';
    private const JWKS_NAME = 'jwks.json';
    private const DISCOVERY_URI = '/.well-known/openid-configuration';

    /** @var string */
    private $hydraServerUrl;

    /** @var string */
    private $cachePath;

    /** @var ClientInterface */
    private $httpClient;

    /** @var array */
    private $configuration;

    /** @var string */
    private $pem;

    /**
     * OIDCDiscovery constructor.
     *
     * @param string $hydraServerUrl
     * @param string $cachePath
     * @param bool $verifySSL
     */
    public function __construct(
        string $hydraServerUrl,
        string $cachePath,
        bool $verifySSL
    ) {
        $this->hydraServerUrl = rtrim($hydraServerUrl, '/');
        $this->cachePath = rtrim($cachePath, "/\\") . '/';
        $this->httpClient = new Client(['verify' => $verifySSL]);
    }

    /**
     * Returns the HTTP client
     *
     * @return ClientInterface
     */
    public function getHttpClient(): ClientInterface
    {
        return $this->httpClient;
    }

    /**
     * @inheritDoc
     * @throws GuzzleException
     */
    public function getIssuer(): string
    {
        return $this->getConfiguration()["issuer"];
    }

    /**
     * @inheritDoc
     * @throws GuzzleException
     */
    public function getAuthorizationEndpoint(): string
    {
        return $this->getConfiguration()["authorization_endpoint"];
    }

    /**
     * @inheritDoc
     * @throws GuzzleException
     */
    public function getTokenEndpoint(): string
    {
        return $this->getConfiguration()["token_endpoint"];
    }

    /**
     * @inheritDoc
     * @throws GuzzleException
     */
    public function getUserinfoEndpoint(): string
    {
        return $this->getConfiguration()["userinfo_endpoint"];
    }

    /**
     * @inheritDoc
     * @throws GuzzleException
     */
    public function getRevocationEndpoint(): string
    {
        return $this->getConfiguration()["revocation_endpoint"];
    }

    /**
     * @inheritDoc
     * @throws GuzzleException
     */
    public function getEndSessionEndpoint(): string
    {
        return $this->getConfiguration()["end_session_endpoint"];
    }

    /**
     * @inheritDoc
     * @return string
     * @throws GuzzleException
     * @throws Base64DecodeException
     * @throws JWKConverterException
     */
    public function getPublicKey(): string
    {
        if (is_null($this->pem)) {
            // If cache exists, resolve from cache
            $jwksPath = $this->cachePath . self::JWKS_NAME;
            if (!file_exists($jwksPath)) {
                $source = $this->getJwksUri();
                $httpClient = $this->getHttpClient();
                $httpClient->request('GET', $source, [
                    RequestOptions::ON_HEADERS => function (ResponseInterface $response) {
                        if ($response->getStatusCode() !== 200) {
                            throw new \Exception('Error while getting public key from remote service.');
                        }
                    },
                    RequestOptions::SINK => $jwksPath
                ]);
            }

            $jwks = file_get_contents($jwksPath);
            $keys = json_decode($jwks, true);
            $jwkConverter = new JWKConverter();
            $this->pem = $jwkConverter->toPEM($keys["keys"][0]);
        }

        return $this->pem;
    }

    /**
     * Returns the JWKS URI
     *
     * @return string
     * @throws GuzzleException
     */
    private function getJwksUri(): string
    {
        return $this->getConfiguration()["jwks_uri"];
    }

    /**
     * Returns the OIDC configuration
     *
     * @return array
     * @throws GuzzleException
     */
    private function getConfiguration(): array
    {
        if (is_null($this->configuration)) {
            // If cache exists, resolve from cache
            $configPath = $this->cachePath . self::CONFIG_NAME;
            if (!file_exists($configPath)) {
                $source = $this->hydraServerUrl . self::DISCOVERY_URI;
                $httpClient = $this->getHttpClient();
                $httpClient->request('GET', $source, [
                    RequestOptions::ON_HEADERS => function (ResponseInterface $response) {
                        if ($response->getStatusCode() !== 200) {
                            throw new \Exception('Error while getting configuration from remote service.');
                        }
                    },
                    RequestOptions::SINK => $configPath
                ]);
            }

            $config = file_get_contents($configPath);
            $this->configuration = json_decode($config, true);
        }

        return $this->configuration;
    }
}
