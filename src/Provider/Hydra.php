<?php


namespace Vallarj\OAuth2\Client\Provider;


use Vallarj\OAuth2\Client\Exception\InvalidTokenException;
use Vallarj\OAuth2\Client\Token\AccessToken as HydraAccessToken;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use League\OAuth2\Client\Grant\AbstractGrant;
use League\OAuth2\Client\Grant\RefreshToken;
use League\OAuth2\Client\OptionProvider\HttpBasicAuthOptionProvider;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Provider\GenericResourceOwner;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Token\AccessTokenInterface;
use League\OAuth2\Client\Tool\BearerAuthorizationTrait;
use Psr\Http\Message\ResponseInterface;

class Hydra extends AbstractProvider
{
    private const RESOURCE_OWNER_ID = 'sub';
    private const RESPONSE_ERROR_KEY = 'error';
    private const RESPONSE_ERROR_CODE_KEY = 'status_code';

    use BearerAuthorizationTrait;

    /** @var OpenIdConfiguration */
    private $configuration;

    /** @var string[] */
    private $scopes;

    /** @var string */
    private $nonce;

    /**
     * Hydra constructor.
     *
     * @param string $clientId
     * @param string $clientSecret
     * @param string $redirectUri
     * @param string[] $scopes
     * @param bool $verifySSL
     * @param OpenIdConfiguration $configuration
     */
    public function __construct(
        string $clientId,
        string $clientSecret,
        string $redirectUri,
        array $scopes,
        bool $verifySSL,
        OpenIdConfiguration $configuration
    ) {
        parent::__construct([
            'clientId' => $clientId,
            'clientSecret' => $clientSecret,
            'redirectUri' => $redirectUri
        ], [
            'optionProvider' => new HttpBasicAuthOptionProvider(),
            'httpClient' => new HttpClient(['verify' => $verifySSL])
        ]);

        $this->scopes = array_keys(
            array_merge(array_flip($scopes), array_flip(['openid', 'offline_access']))
        );

        $this->configuration = $configuration;
    }

    /**
     * @inheritDoc
     */
    public function getBaseAuthorizationUrl()
    {
        return $this->configuration->getAuthorizationEndpoint();
    }

    /**
     * @inheritDoc
     */
    public function getBaseAccessTokenUrl(array $params)
    {
        return $this->configuration->getTokenEndpoint();
    }

    /**
     * Returns the end session endpoint URL
     *
     * @return string
     */
    public function getBaseEndSessionURL(): string
    {
        return $this->configuration->getEndSessionEndpoint();
    }

    /**
     * @inheritDoc
     */
    public function getResourceOwnerDetailsUrl(AccessToken $token)
    {
        return $this->configuration->getUserinfoEndpoint();
    }

    /**
     * @inheritDoc
     */
    protected function getDefaultScopes()
    {
        return $this->scopes;
    }

    /**
     * Override as OIDC specification states that scopes must be separated by spaces.
     *
     * @return string
     */
    protected function getScopeSeparator()
    {
        return ' ';
    }

    /**
     * @inheritDoc
     * @throws IdentityProviderException
     */
    protected function checkResponse(ResponseInterface $response, $data)
    {
        if (!empty($data[self::RESPONSE_ERROR_KEY])) {
            $error = $data[self::RESPONSE_ERROR_KEY];
            if (!is_string($error)) {
                $error = var_export($error, true);
            }

            $code = !empty($data[self::RESPONSE_ERROR_CODE_KEY]) ? $data[self::RESPONSE_ERROR_CODE_KEY] : 0;
            if (!is_int($code)) {
                $code = intval($code);
            }

            throw new IdentityProviderException($error, $code, $data);
        }
    }

    /**
     * @inheritDoc
     */
    protected function createResourceOwner(array $response, AccessToken $token)
    {
        return new GenericResourceOwner($response, self::RESOURCE_OWNER_ID);
    }

    /**
     * Requests an access token using a specified grant and option set
     *
     * @param mixed $grant
     * @param array $options
     * @return AccessTokenInterface|AccessToken
     * @throws IdentityProviderException|InvalidTokenException
     */
    public function getAccessToken($grant, array $options = [])
    {
        $nonce = $options['nonce'] ?? null;
        unset($options['nonce']);

        /** @var HydraAccessToken $accessToken */
        $accessToken = parent::getAccessToken($grant, $options);
        $idToken = $accessToken->getIdToken();

        if (!$grant instanceof RefreshToken && is_null($idToken)) {
            throw new InvalidTokenException("Expected an id_token but did not " .
                "receive one from the authorization server.");
        }

        $publicKey = $this->configuration->getPublicKey();
        $issuer = $this->configuration->getIssuer();
        $audience = $this->clientId;

        if (!is_null($idToken) && !$accessToken->hasValidIdToken($publicKey, $nonce, $issuer, $audience)) {
            throw new InvalidTokenException("Received an invalid id_token from authorization server.");
        }

        return $accessToken;
    }

    /**
     * @inheritDoc
     */
    protected function createAccessToken(array $response, AbstractGrant $grant)
    {
        return new HydraAccessToken($response);
    }

    /**
     * Returns the current value of the nonce parameter.
     *
     * This can be accessed by the redirect handler during authorization.
     *
     * @return string
     */
    public function getNonce(): string
    {
        return $this->nonce;
    }

    /**
     * Revokes an access or refresh token
     *
     * @param HydraAccessToken|string $accessToken
     * @param HydraAccessToken|string $revokeToken
     * @return void
     * @throws GuzzleException
     */
    public function revokeToken($accessToken, $revokeToken): void
    {
        $endpoint = $this->configuration->getRevocationEndpoint();
        $httpClient = $this->getHttpClient();
        $options = [
            RequestOptions::HEADERS => [
                'Accept' => 'application/json',
                'Authorization' => 'Basic ' . base64_encode($this->clientId . ":" . $this->clientSecret)
            ],
            RequestOptions::FORM_PARAMS => [
                'token' => $revokeToken
            ],
        ];

        $httpClient->request("POST", $endpoint, $options);
    }

    /**
     * @inheritDoc
     */
    protected function getAuthorizationParameters(array $options)
    {
        $options = parent::getAuthorizationParameters($options);

        if (empty($options['nonce'])) {
            $options['nonce'] = $this->getRandomState();
        }

        // Store the nonce as it may need to be accessed later on.
        $this->nonce = $options['nonce'];

        return $options;
    }
}
